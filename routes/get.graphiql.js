const {graphiqlKoa} = require('apollo-server-koa');


module.exports = (back) => {

    const router = back.get('router');
    router.get('/graphiql', graphiqlKoa({endpointURL: '/graphql'}));
};
