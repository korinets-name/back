const {graphqlKoa} = require('apollo-server-koa');
const fs = require('fs');
const {makeExecutableSchema} = require('graphql-tools');


module.exports = async (back) => {

    const {password, uuid} = back.services;

    const typeDefs = fs.readFileSync('schema.graphql', 'utf8');
    const resolvers = {
        Query: {
            async password(obj, args, context, info) {
                return await password.generate(args.length);
            },
            async uuid(obj, args, context, info) {
                return await uuid.generate();
            }
        },
    };

    const schema = makeExecutableSchema({
        typeDefs,
        resolvers,
    });

    const router = back.get('router');
    router.post('/graphql', graphqlKoa({schema}));
};
