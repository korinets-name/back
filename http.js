const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');


const http = new Koa();
http.use(bodyParser());
http.use(cors());

module.exports = http;
