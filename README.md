❇️ korinets.name backend
========================

A GraphQL based API behind https://korinets.name/


Run
---

```
npm start
```


Play with GraphQL
-----------------

There is a `graphiql` interface available at `/graphiql` path.
