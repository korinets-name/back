const uuidv4 = require('uuid/v4');


module.exports = async function () {

    const {logger} = this;
    const newID = uuidv4();
    logger.debug(newID);
    return newID;
};
