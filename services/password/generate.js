const generate = require('nanoid/generate');


module.exports = async function (length) {

    const {kojo, logger} = this;
    const config = kojo.get('config');
    const newPassword = generate(config.password.alphabet, length);
    logger.debug(newPassword, length);
    return newPassword;
};
