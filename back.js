const Router = require('koa-router');
const Kojo = require('kojo');
const configLoader = require('yt-config');
const http = require('./http');


module.exports = async () => {

    const config = await configLoader('config.ini');

    const back = new Kojo(config.kojo);
    back.set('config', config);

    const router = new Router();
    http.use(router.routes());
    http.use(router.allowedMethods());
    back.set('router', router);

    await back.ready();
    http.listen(config.http.port);
    return back;
};
